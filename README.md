# Challenge: Liteblog

## Pre-requisitos
Haber completado el tutorial inicial de Nestjs que incluye los siguientes tutoriales
 1. [First steps](https://docs.nestjs.com/first-steps)
 2. [Controllers](https://docs.nestjs.com/controllers)
 3. [Providers](https://docs.nestjs.com/providers)
 4. [Modules](https://docs.nestjs.com/modules)
 5. [Middleware](https://docs.nestjs.com/middleware)
 6. [Exception filters](https://docs.nestjs.com/exception-filters)
 7. [Pipes](https://docs.nestjs.com/pipes)
 8. [Guards](https://docs.nestjs.com/guards)
 9. [Interceptors](https://docs.nestjs.com/interceptors)
 10. [Custom decorators](https://docs.nestjs.com/custom-decorators)

## Parte 1: Configuración de NestJS con dos modelos
Para completar este desafío, deberás seguir los siguientes pasos para configurar un proyecto de NestJS llamado LiteBlog. El proyecto debe tener las siguientes configuraciones:

1. Utiliza la documentación de NestJS para crear un nuevo proyecto llamado LiteBlog.

2. Configura el linter y el formateador de código (linter y prettier) en el proyecto.

3. Configura TypeORM en el proyecto. Utiliza SQLite como base de datos.

4. Crea dos modelos en TypeORM: User y Post.

5. Establece la relación entre los modelos User y Post de la siguiente manera:
   - Un usuario (User) puede tener muchos posts (Post).
   - Un post (Post) pertenece a un usuario (User).

6. Crea las migraciones necesarias para crear los modelos en la base de datos.

7. Utiliza branches en tu repositorio de código para organizar tu trabajo. Crea una rama (branch) llamada "configuracion" donde realizarás las configuraciones iniciales y la creación de los modelos. Luego, crea una rama llamada "migraciones" donde trabajarás en la creación de las migraciones.

8. Utiliza archivos de configuración adecuados para cada aspecto del proyecto, como el archivo de configuración de TypeORM y los archivos de configuración del linter y prettier.

Recuerda que este proyecto debe estar en tu repositorio personal.

Links utiles:
- https://docs.nestjs.com/techniques/database
- https://typeorm.io/migrations#creating-a-new-migration

## Parte 2: Controllers y Services
Para este challenge vamos a crear un base service y un base controller que tengan un crud básico con la idea de evitar la duplicación de código. 
Vamos a crear un modulo llamado *base* que va a tener tanto el base service como el base controller.

### Base service
La clase BaseService debe implementar la siguiente interfaz:
```
export interface BaseService<T> {
    getAll(): Promise<T[]>;
    getOne(id: number): Promise<T>;
    update(entity: T): Promise<T>;
    create(entity: T): Promise<T>;
    delete(id: number);
}
```
Recordar utilizar `import { Repository } from 'typeorm';` para inyectar un repositorio genérico de la siguiente manera en el constructor:
```
constructor(private readonly genericRepository: Repository<T>) {}
```
En la implementación de cada método solo debe aplicar la función correspondiente del ORM. Es decir, evitar agregar validaciones adicionales a los métodos.

### Base controller
La clase BaseController debe inyectar base service y disponibilizar los siguientes endpoints:
```
GET /		| getAll(): Promise<T[]>;
GET /:id	| getOne(id: number):Promise<T>;
POST /		| create(entity: T): Promise<T>;
PUT /		| update(entity: T): Promise<T>;
DELETE /	| delete(id: number);
```
En la implementación de cada endpoint solo debe aplicar el método correspondiente al service. Es decir, evitar agregar validaciones adicionales a los endpoints.

### Implementación del base service
Como siguiente paso de este challenge vamos a utilizar el base service y controller para implementar un CRUD para las entities User y Post. 
Crear dos modulos, uno para user y otro para post, que implementen tanto base service como base controller para tener disponibles sus CRUDs.

Te dejo un link de guía
https://gaiyaobed.hashnode.dev/building-reusable-crud-apis-with-typeorm-and-nestjs


### Parte 3: Documentación con Swagger
La documentación de una API es fundamental para facilitar su comprensión y fomentar el intercambio de conocimientos entre los usuarios. En este caso, utilizaremos Swagger para generar la documentación y una página estática que permitirá probarla. El primer paso consistirá en configurar Swagger en el repositorio, lo que nos permitirá posteriormente añadir decoradores y completar la documentación de forma progresiva.
https://docs.nestjs.com/openapi/introduction

Después de completar la configuración, procederemos a documentar todos los endpoints existentes siguiendo la referencia proporcionada en el siguiente enlace: https://github.com/nestjs/nest/tree/master/sample/11-swagger

**⚠️ Importante**: A partir de este punto todos los nuevos endpoints deben poseer su respectiva documentación.

¡Buena suerte!